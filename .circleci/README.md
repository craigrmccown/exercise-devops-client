# CircleCI Pipeline

This directory holds configuration files that define the CI pipeline for this project.

## The Pipeline

The build pipeline uses CircleCI Workflows to achieve a multi-step, parallel CI pipeline. The pipeline looks like this:

```
              --- test ---
            /              \
install ---                  --- build --- publish --- approve --- deploy
            \              /
              --- lint ---
```

### Jobs

- **install**: Runs `yarn install` to install dependencies and caches them
- **test/lint**: Runs all tests and linter rules against the codebase. Any other static analysis tools, such as type checking, should run in parallel with these jobs.
- **build**: Builds the code and static assets into the build directory
- **publish**: Builds a Docker image containing an Nginx server and the build artifacts and pushes it to a Docker registry
- **approve**: A manual approval step to trigger deployment
- **deploy**: Uses Helm to deploy the built container to a GKE cluster

## Build Environment

These environment variables should be set for the project in CircleCI:

- **DOCKER_PASS**: The password that will be used to authenticate with the Docker registry
- **DOCKER_REPO**: The registry to which the publish job will push built images
- **DOCKER_USER**: The username that will be used to authenticate with the Docker registry
- **GCLOUD_CLUSTER_NAME**: The name of the GKE cluster
- **GCLOUD_JSON_KEY**: The JSON key of a Google Cloud service account that has access to connect to the GKE cluster
- **GCLOUD_PROJECT**: The Google Cloud project that owns the cluster
- **GCLOUD_ZONE**: The Google Cloud zone where the cluster is deployed
- **HELM_VERSION**: The version of Helm that will be used to deploy the application. Should match the version of the cluster's Tiller deployment.
- **KUBE_APP_NAMESPACE**: The Kubernetes namespace to which the application deployment will belong
- **KUBE_INGRESS_HOST**: The host name that will receive ingress requests
- **KUBE_TILLER_NAMESPACE**: The Kubernetes namespace to which Tiller belongs
- **REACT_APP_API_URL**: The url of the API for this application

## Future Work

To improve the CI pipeline:

- The config.yml could be DRYed up using common commands and/or CircleCI orbs.
- Environment variables should be managed in a better way. The current configuration requires config.yml to be changed if a developer adds a new environment variable.
- In reality, most applications need many live environments, such as development and staging. The Helm release name should be templated by the environment to allow for this.
- Docker layer caching could be utilized to speed up build times.
- Depending on the git workflow for this project, a new deployment could be made for every feature branch or pull request. Currently, the pipeline will only run for new commits to master.
