# BW-Client Helm Chart

This chart deploys the bw-client application.

## Installing and Uninstalling

To install this chart, first clone this repository. Then, run:

```console
$ cd /path/to/repo
$ helm install --name release-name ./chart
```

After the chart is installed, you can uninstall it by running:

```console
$ helm delete --purge release-name
```

## Configuration

To configure this chart, pass the `--set key=value` argument to `helm install` or `helm upgrade`.

| Parameter     | Description                                                    | Default                         |
| ------------- | -------------------------------------------------------------- | ------------------------------- |
| image         | The Docker image to use to create the application container    | `craigrmccown/bw-client:latest` |
| host          | The host name to which ingress requests will be made           | `example.com`                   |
| issuerName    | The name of the cert-manager Issuer to use                     | `letsencrypt`                   |
| servicePort   | The internal port on which the service will listen             | `5000`                          |
| containerPort | The internal port on which the container will receive requests | `5000`                          |

## Future Work

To improve this chart:

- Common templating logic can be abstracted to a .tpl file
- Every value should be sanitized and truncated to the correct length
- The Deployment manifest could be more fleshed out. For example, a `livenessProbe` and `readinessProbe` could be added to the container spec.
- Hardcoded values, such as replicas and chart name, could be overridable, allowing this to become a general application deployment chart.
